#!/usr/bin/env python

import os
from engine import Engine
import chess
import argparse
import time

def selfplay(engine):
    board = chess.Board()
    while not board.is_game_over():
        start = time.time()
        move = engine.move(board, {})
        elapsed = (time.time() - start) * 1000
        board.push(move)
        print(board)
        print(f'Best: {move} after {elapsed:0.2f}ms')
        # print(f'Evaluation: {value:10.1f}')

        print('\x1b[9A', end='')

    print(board.result())

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    group = parser.add_argument_group('Main')
    group_mx = group.add_mutually_exclusive_group(required = True)
    group_mx.add_argument('--lichess', help = "serve to lichses")
    group_mx.add_argument('--uci', help = "serve to stdin/stdout via uci", action = 'store_true')
    group_mx.add_argument('--selfplay', help = "play vs itself", action = 'store_true')
    args = parser.parse_args()

    cores = len(os.sched_getaffinity(0))
    with Engine(cores = cores) as engine:
        if args.lichess:
            engine.serve_lichess(args.lichess)
        elif args.uci:
            engine.serve_uci()
        elif args.selfplay:
            selfplay(engine)
