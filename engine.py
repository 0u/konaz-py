import os
import ezengine as ez
import operator as op
import chess
from multiprocessing import Pool
from multiprocessing.pool import AsyncResult

NAME = 'konaz 1.0'
AUTHOR = 'Ulisse Mini'

values = {chess.PAWN: 1,
          chess.KNIGHT: 3,
          chess.BISHOP: 3.1,
          chess.ROOK: 5,
          chess.QUEEN: 9,
          chess.KING: 0}

inf = float('inf')
def value(b: chess.Board) -> int:
    # game over values
    if b.is_game_over():
        if b.result() == "1-0":
            return inf
        elif b.result() == "0-1":
            return -inf
        else:
            return 0

    val = 0.0
    # piece values
    pm = b.piece_map()
    for x in pm:
        tval = values[pm[x].piece_type]
        if pm[x].color == chess.WHITE:
            val += tval
        else:
            val -= tval

    # add a bit of value for amount of legal moves.
    bak = b.turn
    b.turn = chess.WHITE
    val += 00.1 * b.legal_moves.count()
    b.turn = chess.BLACK
    val -= 00.1 * b.legal_moves.count()
    b.turn = bak

    return val

def minimax(v, board: chess.Board, depth: int, alpha = -inf, beta = inf) -> (float, chess.Move):
    """
    Preform the minimax algorythm,
    returning the evaluation for a board given a depth.
    """
    if board.is_game_over():
        return v(board), None

    best_move = next(iter(board.legal_moves))

    if depth <= 0 or board.is_game_over():
        return v(board), best_move

    best_value = -inf if board.turn else inf
    if board.turn == chess.WHITE:
        for move in board.legal_moves:
            board.push(move)
            value, _ = minimax(v, board, depth - 1, alpha, beta)
            board.pop()
            if value > best_value:
                best_value = value
                best_move = move
            alpha = max(alpha, value)
            if beta <= alpha:
                break # prune
    else:
        for move in board.legal_moves:
            board.push(move)
            value, _ = minimax(v, board, depth - 1, alpha, beta)
            board.pop()
            if value < best_value:
                best_value = value
                best_move = move
            beta = min(beta, value)
            if beta <= alpha:
                break # prune

    return best_value, best_move


class Engine(ez.Engine):
    def __init__(self, cores = 1):
        # self.value = ClassicValuator()
        super().__init__()
        self.value = value
        self.cores = cores
        self.pool = None

    def __enter__(self):
        if self.cores > 1:
            self.pool = Pool(self.cores)
        return self

    def __exit__(self, *_):
        if self.pool:
            self.pool.close()

    def uci(self, _):
        print(f'id name {NAME}')
        print(f'id author {AUTHOR}')
        print('uciok')

    def move(self, board: chess.Board, options: dict) -> chess.Move:
        """
        Wrapper around minimax adding multiprocessing if self.cores > 1.
        """

        depth = options.get('depth') or 3
        if board.is_game_over():
            raise ValueError("Game is over")

        if self.pool is not None:
            # AsyncResult's of minimax on board.legal_moves
            results = []
            for move in board.legal_moves:
                board.push(move)
                results.append(
                    self.pool.apply_async(
                        minimax, [self.value, board.copy(), depth - 1])
                )
                board.pop()

            best_value = -inf if board.turn else inf
            best_move = next(iter(board.legal_moves))
            better = op.gt if board.turn else op.lt

            for (move, (value, best_opponent_move)) in zip(board.legal_moves, map(AsyncResult.get, results)):
                if better(value, best_value):
                    #print(f'{move} ({value}) better then: {best_move} ({best_value}) best response: {best_opponent_move}')
                    best_value = value
                    best_move = move

            # Return the best move, and its value.
            return best_move
        else:
            best_value, best_move = minimax(self.value, board, depth)
            return best_move
